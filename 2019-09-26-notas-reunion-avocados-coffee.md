# Resumen reunión :es: :avocado: :coffee: (26, Sep, 2019)
# Asistentes

* @alnacle
* @anajsana
* @Dgomezg
* @ntkog
* @jsmanrique


# Notas

## Topic: Objetivos y roles de un developer advocate

Empezamos hablando de cómo los objetivos de un developer advocate varían según el tamaño de la compañía. Hablamos de temas como recruiting, crear partners, comunidades o usuarios frecuentes del producto

También se mencionó los diferentes perfiles que puede necesitar un developer advocate, y como en equipos grandes de DevRel existen perfiles enfocados a producto, outreach o comunidades. 

Concepto interesantes planteado: [Thoughtful leadership](https://en.wikipedia.org/wiki/Thought_leader)


## Topic: ¿A quién debe reportar un developer advocate?

¿Dónde encaja un developer advocate en la empresa? Es importante saber a quién debe reportar, ya que eso influirá en tus objetivos. Podía ser a marketing, o a producto o incluso estar al mismo nivel.

Una pregunta interesante que se lanzó fue “¿Por qué haces developer advocacy en tu empresa? ¿Cuales fueron sus necesidades de tu empresa en contratar a un developer advocate?” Responder esas preguntas puede que ayude a entender a quién se debe reportar y tener mejor definidos los objetivos a los que se quiere llegar.

Muchas veces nuestro rol nos hace estar en contacto con varios departamentos de nuestra empresa, es transversal.

Interesante la presentación de Mary Thengvall sobre este tema: [Developer avocados, the good kind of fat](https://noti.st/marythengvall/0VfGRi/developer-avocados-the-good-kind-of-fat )

## Topic: Relación amor-odio entre Marketing y Developer Advocate

“A los desarrolladores no les gusta que les cuentes lo genial que es tu producto.” Necesita probar y aprender. Por ello, un *sales pitch* nunca va a encajar en ellos. Sin embargo, lo que sí es importante es darse a conocer y que de lo que hables vaya acorde con el tema principal del evento.

## Topic: ¿Cómo medir el retorno en los eventos?

Dependía de si la razón por la que ibas al evento/meetup estaba más ligado a awareness o a ayudar a desarrolladores que ya utilizaban/sabían de tu producto. Se destacó que la parte de awareness era un proceso lento y que el retorno en ese caso tardaba mucho en verse

## Topic: Próximo avocado e-café

Como tema inicial para nuestro segundo día, queríamos hablar de comunidades: ¿Qué es comunidad? 

Podéis añadir temas concretos de los que queráis hablar para el próximo jueves y así tener una agenda. Por el momento, los que se mencionaron fueron:

Evolución del concepto comunidad
El rol de la publicidad en comunidades hoy en día

# Documentos / Referencias / Contexto

Videos:
* Matthew Revell & Carla Sofia Teixeira - [The four archetypes of developer champion programmes](https://www.youtube.com/watch?v=9aIH0AyKd6M)
* Justin Johnson, Bear Douglas, Jade Wang, and Tim Falls [Building Dev Rel Teams](https://www.youtube.com/watch?time_continue=1138&v=mH8nmMxhHoc)
* DevGuild Dev Evangelism Meetup: [Developer Evangelism Panel](https://www.youtube.com/watch?time_continue=510&v=11X1M0qnHnc)


# Acciones

